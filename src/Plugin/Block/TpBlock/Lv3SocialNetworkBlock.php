<?php

namespace Drupal\training_correction\Plugin\Block\TpBlock;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Correction for TP Block - Level 2.
 *
 * @Block(
 *  id = "block_tp_lv3",
 *  admin_label = @Translation("TP Block 3 : Social networks"),
 * )
 */
class Lv3SocialNetworkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The link generator service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('link_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $config,
    LinkGeneratorInterface $link_generator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
    $this->linkGenerator = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $social_network_config = $this->config->get('training_correction.settings');
    $items = [];
    foreach ($social_network_config->getRawData() as $label => $link) {
      $url = Url::fromUri($link);
      $items[] = $this->linkGenerator->generate($label, $url);
    }
    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

}
