<?php

namespace Drupal\training_correction\Controller\TpForm;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Correction for TP Form - Level 2.
 *
 * Class FormRedirectionController.
 */
class FormRedirectionController extends ControllerBase {

  /**
   * The request manager service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestManager;

  /**
   * The private tempstore factory services.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The temp store.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('request_stack'),
        $container->get('tempstore.private'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_manager, PrivateTempStoreFactory $temp_store_factory) {
    $this->requestManager = $request_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('training_correction');
  }

  /**
   * Manage redirection with query parameters.
   */
  public function formRedirect() {

    $params = $this->requestManager->getCurrentRequest()->query->all();
    $name = \array_key_exists('name', $params) ? $params['name'] : '';
    $mail = \array_key_exists('mail', $params) ? $params['mail'] : '';
    $topic = \array_key_exists('topic', $params) ? $params['topic'] : '';
    $list = [
      $this->t('Name:') . ' ' . $name,
      $this->t('Email:') . ' ' . $mail,
      $this->t('Topic:') . ' ' . $topic,
    ];

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('Thank you for your subscription') . '</h1>',
    ];
  }

  /**
   * Manage redirection using a temporary storage.
   */
  public function formRedirectUsingTempStore() {

    $list = [];
    $datas = $this->store->get('newsletters_subscription_form_data');
    $this->store->delete('newsletters_subscription_form_data');

    if (!\is_null($datas)) {
      $name = \array_key_exists('name', $datas) ? $datas['name'] : '';
      $mail  = \array_key_exists('mail', $datas) ? $datas['mail'] : '';
      $topic = \array_key_exists('topic', $datas) ? $datas['topic'] : '';
      $list = [
        $this->t('Name:') . ' ' . $name,
        $this->t('Email:') . ' ' . $mail,
        $this->t('Topic:') . ' ' . $sport,
      ];
    }

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('Thank you for your subscription') . '</h1>',
    ];
  }

  /**
   * TP Form : Level 3.
   */
  public function redirectionNewsletter() {
    return [
      '#markup' => 'Inscription confirmée',
    ];
  }

}
