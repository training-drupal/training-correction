<?php

namespace Drupal\training_correction\Controller\TpController;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableJsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Correction for TP Controller - Level 1.
 *
 * Class Lv2JsonResponseController.
 */
class Lv2JsonResponseController extends ControllerBase {

  /**
   * The serialization service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('serialization.json'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(Json $serializer) {
    $this->serializer = $serializer;
  }

  /**
   * Return a json of all pokemon.
   */
  public function response() {
    // Search all pokemon.
    // return full entities.
    $pokemons = $this->entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'pokemon',
      ]
    );

    // Foreach pokemons.
    $results = [];
    foreach ($pokemons as $pokemon) {
      // Get datas.
      // Type.
      $type_to_encode = [];

      if (!$pokemon->get('field_types')->isEmpty()) {
        $types = $pokemon->get('field_types')
          ->referencedEntities();
        foreach ($types as $type) {
          $type_to_encode[] = $type->getName();
        }
      }
      $json_types = $this->serializer->encode($type_to_encode);

      // Pokedex id.
      $id = '';
      if (!$pokemon->get('field_id')->isEmpty()) {
        $field_id = $pokemon->get('field_id')
          ->first()
          ->getValue();
        $id = $field_id['value'];
      }

      // Create row.
      $results[] = [
        $pokemon->getTitle(),
        $id,
        $json_types,
        $pokemon->getChangedTime(),
      ];
    }

    // Create and return Json.
    $json = $this->serializer->encode($results);

    return new CacheableJsonResponse($json);
  }

}
